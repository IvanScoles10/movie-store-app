import axios from 'axios';
const API = 'https://api.themoviedb.org/3';
const API_KEY = '9967bb2663b93d55807b47213437ba9e';

const getDiscoverMovies = (sortBy, search) => {
    const query = (search) ?
        `${API}/search/movie?api_key=${API_KEY}&language=en-US&sort_by=${sortBy}&query=${search}&include_adult=false&include_video=false&page=1` : 
        `${API}/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=${sortBy}&include_adult=false&include_video=false&page=1` 

    return axios.get(
        query,
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    );
};

module.exports = {
    getDiscoverMovies,
}