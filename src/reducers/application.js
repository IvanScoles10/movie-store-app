import { SET_SEARCH, CLEAR__SEARCH } from '../constants/actions';

export default function(
    state = {
        search: null,
    },
    { type, payload },
) {
    switch (type) {
        case SET_SEARCH:
            const { search } = payload;
            
            return {
                ...state,
                search,
            };
        case CLEAR__SEARCH:
            return {
                ...state,
                search: null,
            };
        default:
            return state;
    }
}