import { combineReducers } from 'redux';
import application from './application';

const combinedReducers = combineReducers({
    application
});

export default combinedReducers;
