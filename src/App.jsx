import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import { ROUTES } from './constants/routes';

import DiscoverPage from './components/discover'
import Navigation from './components/navigation'

const App = () => (
    <Router>
        <Navigation />
        <Container>
            <Route
                exact
                path={ROUTES.DISCOVER}
                component={DiscoverPage}
            />
        </Container>
    </Router>
);

export default App;
