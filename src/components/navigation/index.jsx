import React, { useState } from 'react';
import { connect } from 'react-redux';
import { SET_SEARCH } from '../../constants/actions'

import { FormControl, Button, Nav, Navbar } from 'react-bootstrap'

const Navigation = ({ setSearchChange }) => {
    const [search, setSearch] = useState('');

    const onSearchChange = (e) => {
        if (e.target.value && e.target.value.length > 0) {
            setSearch(e.target.value);
        } else {
            setSearchChange(null);
        }
    }
    const handleSearch = () => {
        setSearchChange(search);
    };

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="#home">Movie-Store</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="#discover">Discover</Nav.Link>
                </Nav>
                <div className="form-inline">
                    <FormControl
                        type="text"
                        placeholder="Search"
                        className="mr-sm-2"
                        onChange={(e) => onSearchChange(e)}
                    />
                    <Button variant="outline-success" onClick={handleSearch}>Search</Button>
                </div>
            </Navbar.Collapse>
        </Navbar>
    );
};

const mapDispatchToProps = dispatch => {
    return {
        setSearchChange: payload =>
            dispatch({
                type: SET_SEARCH,
                payload: {
                    search: payload
                },
            }),
    };
};

export default connect(null, mapDispatchToProps)(Navigation);
