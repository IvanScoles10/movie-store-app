import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { getDiscoverMovies } from '../../services/MovieService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'

import Rating from 'react-rating';


import { Row } from 'react-bootstrap';
import CardMovie from '../card-movie';
import CardMovieModal from '../card-movie-modal';

import './styles.scss';

const Discover = ({ search }) => {
    const [sortBy, setSortBy] = useState('popularity.desc');
    const [movies, setMovies] = useState([]);
    const [movie, setMovie] = useState(null);
    const [rating, setRating] = useState(null);
    let filtered = movies;

    useEffect(() => {
        const fetchDiscoverData = async () => {
            const response = search
                ? await getDiscoverMovies(sortBy, search)
                : await getDiscoverMovies(sortBy);

            if (response && response.data) {
                const { results } = response.data;

                setMovies(results);
            }
        };

        fetchDiscoverData();
    }, [search, sortBy]);

    const handleFilterByRating = (rating) => {
        setRating(rating);
    };

    const handleCloseModal = () => {
        setMovie(null);
    };

    const handleSelectedMovie = (movie) => {
        setMovie(movie);
    };

    if (rating) {
        filtered = filtered.filter((movie) => {
            return movie.vote_average <= rating
        });
    }

    return (
        <div className="discover">
            <Row>
                <div className="discover__filter">
                    <span>Filter by rating</span>
                    <span className="discover__filter-rating">
                        <Rating
                            step={2}
                            stop={10}
                            onClick={handleFilterByRating}
                            emptySymbol={<FontAwesomeIcon icon={faStar} />}
                            fullSymbol={<FontAwesomeIcon icon={faStar} color="red" />}
                        />
                    </span>
                </div>
            </Row>
            <Row>
                {filtered.map((movie, index) => {
                    return (
                        <div
                            className="discover__card-movie"
                            key={`movie-${index}`}
                        >
                            <CardMovie
                                movie={{
                                    src: movie.poster_path,
                                    title: movie.original_title,
                                    overview: movie.overview,
                                    releaseDate: movie.release_date,
                                }}
                                handleSelected={handleSelectedMovie}
                            />
                        </div>
                    );
                })}
            </Row>
            {movie && (
                <CardMovieModal
                    handleClose={handleCloseModal}
                    show={(movie) ? true : false}
                    movie={{
                        src: movie.src,
                        title: movie.title,
                        overview: movie.overview,
                        releaseDate: movie.releaseDate
                    }} />
            )}
        </div>
    );
};

const mapStateToProps = state => {
    return {
        search: state.application.search,
    };
};

export default connect(mapStateToProps, null)(Discover);
