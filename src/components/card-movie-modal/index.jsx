import React from 'react';
import PropTypes from 'prop-types';

import { Modal } from 'react-bootstrap';

import './styles.scss';

const CardMovieModal = ({ handleClose, show, movie }) => {
    const { src, title, overview, releaseDate } = movie;

    return (
        <Modal
            className="card-movie-modal"
            size="lg"
            show={show}
            onHide={handleClose}
            aria-labelledby="example-modal-sizes-title-lg"
        >
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-lg">
                    {title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="card-movie-modal__image">
                    <img src={`https://image.tmdb.org/t/p/w500/${src}`} />
                </div>
                <p>{overview}</p>
                <p>Release date: {releaseDate}</p>
            </Modal.Body>
        </Modal>
    );
};

CardMovieModal.propTypes = {
    handleClose: PropTypes.func,
    show: PropTypes.bool,
    movie: PropTypes.shape({
        src: PropTypes.string,
        title: PropTypes.string,
        overview: PropTypes.string,
        releaseDate: PropTypes.string,
    }),
};

export default CardMovieModal;


