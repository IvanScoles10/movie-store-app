import React from 'react';
import PropTypes from 'prop-types';

import { Card, Button } from 'react-bootstrap';

import './styles.scss';

const CardMovie = ({ handleSelected, movie }) => {
    const { src, title, overview } = movie;
    return (
        <Card className="card-movie">
            <Card.Img variant="top" src={`https://image.tmdb.org/t/p/w500/${src}`} />
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>
                    {overview.length > 100 ? (overview.substring(0, 100) + '...') : overview}
                </Card.Text>
                <Button variant="primary" onClick={() => handleSelected(movie)}>View details</Button>
            </Card.Body>
        </Card>
    );
};

CardMovie.propTypes = {
    movie: PropTypes.shape({
        src: PropTypes.string,
        title: PropTypes.string,
        overview: PropTypes.string,
        releaseDate: PropTypes.string,
    }),
    handleSelected: PropTypes.func,
};

export default CardMovie;
