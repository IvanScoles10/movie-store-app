# Movie Store App [![GitHub license](https://img.shields.io/badge/license-ISC-lime.svg)](https://github.com/tarmac/react-webpack4-boilerplate)

A simple and complete Movie Store App.

* Ready to start React App :zap:
* Image loader configured :camera:
* SASS and CSS loaders configured :art:
* Hot reload configured :sunny:
* Unit test tools configured :microscope:
* Production build optimized :chart_with_upwards_trend:
* Eslint rules configured with [Airbnb React/JSX Style Guide](https://github.com/airbnb/javascript/tree/master/react)
* Precommit hook with [husky](https://github.com/typicode/husky) (run eslint rules and prettier before commit changes)

## Getting Started

To start you can simply [download](https://github.com/tarmac/react-webpack4-boilerplate/archive/master.zip) the boilerplate and unzip it into your working directory. You can also clone if you want to contribute.

### Prerequisites

* Node.js v8 or above

You can check your node version using the command:

```CLI
node --version
```

### Installing

Install dependencies and start using [yarn](https://yarnpkg.com):

```CLI
yarn install

yarn start
```

Or via [npm](https://www.npmjs.com/):

```CLI
npm install

npm start
```

### Testing

You can run your tests using:

```CLI
yarn test
```

It´s possible to generate the code coverage of your code. Jest will generate a HTML file with all information from your tests. To do this run the command:

```CLI
yarn test:coverage
```

### Linter

Linters like eslint can detect potential bugs, as well as code that is difficult to maintain.

```CLI
npm run eslint
```

### Production build

You can generate an optimized distribution bundle. To do this run the command:

```CLI
yarn build
```

It´s possible to check the size and content of your bundled file. To do this run the command:

```CLI
yarn analyze
```

### Linter Rules

Rules can be checked / changed here [.eslintrc](https://github.com/tarmac/react-webpack4-boilerplate/blob/master/.eslintrc)

**General**

[indent](https://en.wikipedia.org/wiki/Indentation_style) 4 spaces

**Jest**

[no-disabled-tests](https://github.com/jest-community/eslint-plugin-jest/blob/master/docs/rules/no-disabled-tests.md)

**React**

[indent](https://en.wikipedia.org/wiki/Indentation_style) 4 spaces

[prefer-stateless-function](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/prefer-stateless-function.md)

```
// Bad
class App extends Component {
    render() {
        return (
            <div className="App">
                <h1>Welcome to React</h1>
            </div>;
        );
    }
}

// Good
const App = () => (
    <div className="App">
        <h1>Welcome to React</h1>
    </div>
);
```

[closing-comp](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/self-closing-comp.md)

```
// Bad
var HelloJohn = <Hello name="John"></Hello>;

// Good
var HelloJohn = <Hello name="John" />;
```

[jsx-closing-bracket-location](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-closing-bracket-location.md)

```
// Bad
<Hello
    lastName="Smith"
    firstName="John" />;

<Hello
    lastName="Smith"
    firstName="John"
  />;
  
// Good
<Hello firstName="John" lastName="Smith" />;

<Hello
    firstName="John"
    lastName="Smith"
/>;
```

[jsx-wrap-multilines](https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-wrap-multilines.md)

```
// Bad
var Hello = createReactClass({
    render: function() {
        return (<div>
            <p>Hello {this.props.name}</p>
        </div>);
    }
});

// Good
var singleLineJSX = <p>Hello</p>

var Hello = createReactClass({
    render: function() {
        return (
            <div>
                <p>Hello {this.props.name}</p>
            </div>
        );
    }
});
```

## Built With

*  [webpack](https://webpack.js.org/) - Static module bundler
*  [React](https://babeljs.io/) - JavaScript library for building user interfaces
*  [Babel](https://babeljs.io/) - EcmaScript Transpiler
*  [Yarn](https://yarnpkg.com) - Dependency Management
*  [Jest](https://jestjs.io/) - JavaScript Testing
*  [Enzyme](https://airbnb.io/enzyme/docs/api/) - Component tests for React

## Versions

* v1.0 - A complete webpack 4 configuration